var cbbh = require("./exchange-rate-retriever-cbbh");
var hnb = require("./exchange-rate-retriever-hnb");

cbbh.findExchangeRate("HRK", "2019-03-01", "Middle").then((rate) => {
    console.log("Done with CBBH:" + rate);
});

hnb.findExchangeRate("BAM", "2019-03-01", "Middle").then((rate) => {
    console.log("Done with HNB:" + rate);
});