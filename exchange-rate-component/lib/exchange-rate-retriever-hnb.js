"use strict"

const rp = require("request-promise");




module.exports = {
    findExchangeRate: (currency, date, type) => {
        return new Promise(function (resolve, reject) {
            console.log(`Processing request for type "${type}", currency "${currency}" on date "${date}".`);

            let conversionTypes = { 
                "Buying": "Kupovni za devize",
                "Middle": "Srednji za devize",
                "Selling": "Prodajni za devize"
            };

            let url = `http://api.hnb.hr/tecajn/v1?valuta=${currency}&datum=${date}`;
            let rate = 0;

            rp.get(url, { json: true },
                (err, res, body) => {
                    rate = body[0][conversionTypes[type]];
                }
            ).then(() => {
                return resolve(rate);
            }).catch((err) => {
                return reject(err);
            });
        });
    }
};