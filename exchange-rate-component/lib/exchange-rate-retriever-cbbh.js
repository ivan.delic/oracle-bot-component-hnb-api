"use strict"

const rp = require("request-promise");
const nameItems = "CurrencyExchangeItems";
const nameCode = "AlphaCode";


module.exports = {
    findExchangeRate: (currency, date, type) => {
        return new Promise(function (resolve, reject) {
            console.log(`Processing request for type "${type}", currency "${currency}" on date "${date}".`);
            
            let conversionTypes = { 
                "Buying": "Buy",
                "Middle": "Middle",
                "Selling": "Sell"
            };
    
            let url = `https://www.cbbh.ba/CurrencyExchange/GetJson?date=${date}`;
            let rate = 0;
    
            rp.get(url, { json: true },
                (err, res, body) => {
                    let rates = body[nameItems];
                    for (let i = 0; i < rates.length; i++)
                        if (rates[i][nameCode] === currency)
                            rate = rates[i][conversionTypes[type]];
                }
            ).then(() => {
                return resolve(rate);
            }).catch((err) => {
                return reject(err);
            });
        });
    }
}