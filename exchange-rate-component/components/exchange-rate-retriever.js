"use strict"

const errHnb = require("../lib/exchange-rate-retriever-hnb");

module.exports = {
 
    metadata: () => ({
        "name": "ExchangeRateRetriever",
        "properties": {
            "output": { "type": "string", "required": true },
            "currency": { "type": "string", "required": true },
            "date": { "type": "string", "required": true },
            "type": { "type": "string", "required": true }
        },
        "supportedActions": [
            "exchangeSuccess",
            "exchangeNotFound",
            "error"
        ]
    }),
 
    invoke: (conversation, done) => {
        let currency = conversation.properties()["currency"];
        let date = conversation.properties()["date"];
        let type = conversation.properties()["type"];
        let output = conversation.properties()["output"];

        try {
            date = new Date(date).toISOString().substring(0,10);
        } 
        catch (error) {
            conversation.transition("error");
            return done();
        }

        errHnb.findExchangeRate(currency, date, type).then((rate) => {
            let answer = !!output ? output.replace("{currency}", currency).replace("{date}", date).replace("{rate}", rate) : rate;
            conversation.reply(answer);
            conversation.keepTurn(true);
            conversation.transition("exchangeSuccess");
            return done();
        }).catch((err) => {
            console.log("Error:" + err);
            conversation.transition("exchangeNotFound");
            return done();
        });
    }
};
