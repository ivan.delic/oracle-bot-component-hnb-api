var botCbbh = require("./exchange-rate-retriever-cbbh");
var botHnb = require("./exchange-rate-retriever");

//console.log(bot.metadata());

var conversation = {
    properties: function() {
        return {
            currency: "EUR",
            date: "2019-03-01",
            type: "Middle",
            output: "aaa {currency} {date} {rate}"
        }
    },
    
    transition: function() {
        return {
        }
    },

    reply: function(answer) {
        console.log(answer);
        return {
        }
    }
}

var done = function() {};

botCbbh.invoke(conversation, done);

botHnb.invoke(conversation, done);